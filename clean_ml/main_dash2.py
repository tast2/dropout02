import dash
import dash_bootstrap_components as dbc
import dash_html_components as html
import plotly.express as px
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import pandas as pd
from pycaret.classification import *
import pycaret.classification as clf
app = dash.Dash(external_stylesheets=[dbc.themes.FLATLY])

model = clf.load_model('dropout02\\clean_ml\\model1')


df=pd.read_excel('dropout02\\clean_ml\\total.xlsx')
dff=pd.read_excel('dropout02\\clean_ml\\data.xlsx')
major=['วิศวกรรมการผลิต','วิศวกรรมคอมพิวเตอร์','วิศวกรรมเคมี','วิศวกรรมเครื่องกล','วิศวกรรมชีวการแพทย์','วิศวกรรมไฟฟ้า','วิศวกรรมเมคาทรอนิกส์','วิศวกรรมโยธา','วิศวกรรมวัสดุ','วิศวกรรมสิ่งแวดล้อม','วิศวกรรมเหมืองแร่','วิศวกรรมอุตสาหการ']
year=[2559,2560,2561,2562,2563,2564]


top=df['PROVINCE_NAME_THAI'].value_counts().head(10)
major={240:'วิศวกรรมคอมพิวเตอร์ ',215:'วิศวกรรมเครื่องกล',210:'วิศวกรรมไฟฟ้า',220:'วิศวกรรมโยธา',230:'วิศวกรรมเคมี',225:'วิศวกรรมอุตสาหการ',226:'วิศวกรรมการผลิต',216:'วิศวกรรมเมคาทรอนิกส์',200:'ยังไม่แยกสาขาวิชา',205:'วิศวกรรมสิ่งแวดล้อม'}
#data คนแต่ละสาขา

#top 10 จังหวัด ที่มีคนมาเรียนที่ มอ 
province=['สงขลา','นครศรีธรรมราช','ตรัง','ยะลา','สุราษฎร์ธานี','ปัตตานี','นราธิวาส','พัทลุง','กรุงเทพมหานคร','ภูเก็ต']
p_province=[1208,484,341,302,283,206,198,178,140,128]

#top 10 โรงเรียน ที่มีคนเข้ามาเรียนที่ มอ
school=['โรงเรียนแสงทองวิทยา','โรงเรียนมหาวชิราวุธ จังหวัดสงขลา','โรงเรียนหาดใหญ่วิทยาลัย','โรงเรียนหาดใหญ่วิทยาลัยสมบูรณ์กุลกันยา','โรงเรียนหาดใหญ่วิทยาลัย 2','โรงเรียนวรนารีเฉลิม จังหวัดสงขลา','โรงเรียนพัทลุง','โรงเรียนกัลยาณีศรีธรรมราช','โรงเรียนสุราษฎร์พิทยา','โรงเรียนคณะราษฎรบำรุง จังหวัดยะลา']
score=[171,158,133,117,80,73,72,67,63,63]

status=df[df['STUDY_STATUS']!='OK']
fig_hit=px.histogram(status, x='ADMIT_YEAR', color='STUDY_STATUS', color_discrete_map={'G': '#FFBD59', 'R': '#3BA27A'}, marginal="box", nbins=30, opacity=0.6, template="plotly_white")

fig_ring=px.pie(df,values=score,names=school)

fig_bar=px.bar(df,x=province,y=p_province)

fig_Scart=px.scatter(dff,x='ENG_SCORE',y='MAJOR_ID',color='STUDY_STATUS')

import dash_bootstrap_components as dbc

navbar = dbc.NavbarSimple(
    brand="Grade Prediction",
    color="LUX",
    dark=True,style={'margin-left': '1px'})
sidebar = html.Div(
    [
        
        dbc.Row(
            [
                html.Div(
                    [
                        html.P('Enter major',
                               style={'margin-top': '8px','color':'black', 'margin-bottom': '4px'},
                               className='font-weight-bold'
                              ),
                        dcc.Dropdown(id='MAJOR_ID', multi=False, value='major',
                                     options=major,
                                     style={'width': '320px'}
                                    ),
                    #------------------------------------
                    html.P('Admit year',
                               style={'margin-top': '8px','color':'black', 'margin-bottom': '4px'},
                               className='font-weight-bold'
                              ),
                        dcc.Dropdown(id='ADMIT_YEAR', multi=False, value='year',
                                     options=year,
                                     style={'width': '320px'}
                                    ),
                        html.P('Enter student ID',
                               style={'margin-top': '8px','color':'black', 'margin-bottom': '4px'},
                               className='font-weight-bold'
                              ),
                        dcc.Input(id='STUDENT_ID', 
                                     style={'width': '320px'}
                                    ),

                    #-----------input text--------------
                        html.P('Input Grate',
                               style={'margin-top': '8px','color':'black', 'margin-bottom': '4px'},
                               className='font-weight-bold'),
                        dcc.Input(id='ENG_SCORE', type='number',placeholder='คะแนนสอบ Eng',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y1T1', type='number',step=0.01,min=0,max=4,placeholder='เกรดปี 1 เทอม 1',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y1T2', type='number',step=0.01,min=0,max=4,placeholder='เกรดปี 1 เทอม 2',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y1T3', type='number',step=0.01,min=0,max=4,placeholder='เกรดปี 1 เทอม 3',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y2T1', type='number',step=0.01,min=0,max=4,placeholder='เกรดปี 2 เทอม 1',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y2T2', type='number',step=0.01,min=0,max=4,placeholder='เกรดปี 2 เทอม 2',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y2T3', type='number',step=0.01,min=0,max=4,placeholder='เกรดปี 2 เทอม 3',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y3T1', type='number',step=0.01,min=0,max=4,placeholder='เกรดปี 3 เทอม 1',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y3T2', type='number',step=0.01,min=0,max=4,placeholder='เกรดปี 3 เทอม 2',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y3T3', type='number',step=0.01,min=0,max=4,placeholder='เกรดปี 3 เทอม 3',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y4T1', type='number',step=0.01,min=0,max=4,placeholder='เกรดปี 4 เทอม 1',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y4T2', type='number',step=0.01,min=0,max=4,placeholder='เกรดปี 4 เทอม 2',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y4T3', type='number',step=0.01,min=0,max=4,placeholder='เกรดปี 4 เทอม 3',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        html.Button('Predict', id='button', n_clicks=0,
                                    style={'width': '320px','color':'warning','margin-bottom': '4px'}),
                        html.Div(id='output'),
                        html.Img(src='static\de_pre.png', style={'width': '300px','margin-bottom': '4px'}),
                        dbc.Card(
                dbc.CardBody(
                    [html.Label('Advice'),
                html.Div(
                [   html.P( className='font-weight-bold'),html.Label('- ถ้าคุณกำลังเรียนอยู่แล้วไม่มีเกรดในเทอมนั้นๆช่องนั้นว่างไว้')
                 ],style={'padding': '20px','border': '1px solid #ddd','border-radius': '10px'} ),]),style={'margin-bottom': '4px'})




                    ]
                )
            ],
            style={"height": "155vh"}
        ),
       
    ]
)
content = html.Div(
    [
        dbc.Row(
            [
                dbc.Col(
                    [
                    dbc.Card(
                dbc.CardBody(
                    [html.Label('Educational status'),
                html.Div(
                [   
                    html.P('Target แต่ละปี', className='font-weight-bold'),
                    dcc.Graph(figure=fig_hit, style={'width':400})
                ],
                style={
                    'padding': '20px',
                    'border': '1px solid #ddd',
                    'border-radius': '10px'
                } ),]),style={'margin': '20px'})]),
                dbc.Col(
                    [
                    dbc.Card(
                dbc.CardBody(
                    [html.Label('Top 10 students who study at psu the most'),
                html.Div(
                [   
                    html.P('Target แต่ละปี', className='font-weight-bold'),
                    dcc.Graph(figure=fig_bar, style={'width':400,'backgroundColor': '#F9F9F9'})
                ],
                style={
                    'padding': '20px',
                    'border': '1px solid #ddd',
                    'border-radius': '10px'
                     }
                    
                    
                 ),]),style={'margin': '20px'})]),
                dbc.Col(
                    [
                    dbc.Card(
                dbc.CardBody(
                    [html.Label('Top 10 schools where students graduated'),
                html.Div(
                [   
                    html.P('TTop 10 schools where students graduated', className='font-weight-bold'),
                    dcc.Graph(figure=fig_ring, style={'width':1000})
                ],
                style={
                    'padding': '20px',
                    'border': '1px solid #ddd',
                    'border-radius': '10px'
                } ),]),style={'margin': '20px'})]),
                
                
            ],
            style={"height": "20vh",'margin-bottom':'300px'}),
        ]
    )

app.layout = dbc.Container(
    [
        dbc.Row(
            [
                navbar,
                dbc.Col(sidebar, width=3, className='bg-light'),
                dbc.Col(content, width=9)
     
            
                
                ]
            ),
        ],
    fluid=True
    )
resultG=html.P('คุณมีแนวโน้มที่จะเรียนจบ',style={'margin-top': '8px','color':'green', 'margin-bottom': '4px'},
                               className='font-weight-bold'
                              )
resultR=html.P('คุณมีแนวโน้มที่จะตกออก',style={'margin-top': '8px','color':'red', 'margin-bottom': '4px'},
                               className='font-weight-bold'
                              )
@app.callback(
    Output('output', 'children'),
    [Input('button', 'n_clicks')],
    [Input('ADMIT_YEAR','value'),
    Input('MAJOR_ID','value'),
    Input('ENG_SCORE','value'),
    Input('Y1T1', 'value'),
    Input('Y1T2', 'value'),
    Input('Y1T3', 'value'),
    Input('Y2T1', 'value'),
    Input('Y2T2', 'value'),
    Input('Y2T3', 'value'),
    Input('Y3T1', 'value'),
    Input('Y3T2', 'value'),
    Input('Y3T3', 'value'),
    Input('Y4T1', 'value'),
    Input('Y4T2', 'value'),
    Input('Y4T3', 'value')])

def predict_grade(n_clicks,ADMIT_YEAR,MAJOR_ID,ENG_SCORE,Y1T1, Y1T2,Y1T3 ,Y2T1,Y2T2, Y2T3,Y3T1, Y3T2, Y3T3,Y4T1, Y4T2,Y4T3):
    if not n_clicks:
        return "Enter Grade"
    else:
        input_data = pd.DataFrame({'ADMIT_YEAR': [ADMIT_YEAR] ,'MAJOR_ID': [MAJOR_ID],'ENG_SCORE':[ENG_SCORE],
            'Y1T1': [Y1T1], 'Y1T2': [Y1T2], 'Y1T3': [Y1T3], 'Y2T1': [Y2T1], 
            'Y2T2': [Y2T2], 'Y2T3': [Y2T3], 'Y3T1': [Y3T1], 'Y3T2': [Y3T2], 
            'Y3T3': [Y3T3], 'Y4T1': [Y4T1], 'Y4T2': [Y4T2], 'Y4T3': [Y4T3]})
        prediction = predict_model(model, data=input_data)
        result=prediction["prediction_label"][0]
        if result == 'G' or result == 'OK':
            return resultG
        else:
            return resultR



if __name__ == "__main__":
    app.run_server(debug=True, port=1234)
