import dash
import dash_bootstrap_components as dbc
import dash_html_components as html
import plotly.express as px
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
from pycaret.classification import *
import pycaret.classification as clf
app = dash.Dash(external_stylesheets=[dbc.themes.FLATLY])

model = clf.load_model('test_model1')


df=pd.read_excel('C:\\Users\\User\\Desktop\\project\\dash_1\\dashboard_dash_plotly_en\\total.xlsx')
dff=pd.read_excel('C:\\Users\\User\\Desktop\\project\\dash_1\\dashboard_dash_plotly_en\\data.xlsx')
major=['วิศวกรรมการผลิต','วิศวกรรมคอมพิวเตอร์','วิศวกรรมเคมี','วิศวกรรมเครื่องกล','วิศวกรรมชีวการแพทย์','วิศวกรรมไฟฟ้า','วิศวกรรมเมคาทรอนิกส์','วิศวกรรมโยธา','วิศวกรรมวัสดุ','วิศวกรรมสิ่งแวดล้อม','วิศวกรรมเหมืองแร่','วิศวกรรมอุตสาหการ']
year=[2559,2560,2561,2562,2563,2564]

#top 10 จังหวัด ที่มีคนมาเรียนที่ มอ 
top=df['PROVINCE_NAME_THAI'].value_counts().head(10)
major={240:'วิศวกรรมคอมพิวเตอร์ ',215:'วิศวกรรมเครื่องกล',210:'วิศวกรรมไฟฟ้า',220:'วิศวกรรมโยธา',230:'วิศวกรรมเคมี',225:'วิศวกรรมอุตสาหการ',226:'วิศวกรรมการผลิต',216:'วิศวกรรมเมคาทรอนิกส์',200:'ยังไม่แยกสาขาวิชา',205:'วิศวกรรมสิ่งแวดล้อม'}


fig_hit=px.histogram(df, x='ADMIT_YEAR', color='STUDY_STATUS', marginal="box", nbins=30 ,opacity=0.6, 
                         color_discrete_sequence=['#FFBD59', '#3BA27A','#000066'],template="plotly_white")


sidebar = html.Div(
    [
        dbc.Row(
            [
                html.H5('Predict', 
                        style={'margin-top': '12px', 'margin-left': '24px'})
            ],
            style={"height": "5vh"},
            className='bg-primary text-white font-italic'
        ),
        dbc.Row(
            [
                html.Div(
                    [
                        html.P('Enter major',
                               style={'margin-top': '8px', 'margin-bottom': '4px'},
                               className='font-weight-bold'
                              ),
                        dcc.Dropdown(id='MAJOR_ID', multi=False, value='major',
                                     options=major,
                                     style={'width': '320px'}
                                    ),
                    #------------------------------------
                    html.P('Admit year',
                               style={'margin-top': '8px', 'margin-bottom': '4px'},
                               className='font-weight-bold'
                              ),
                        dcc.Dropdown(id='ADMIT_YEAR', multi=False, value='year',
                                     options=year,
                                     style={'width': '320px'}
                                    ),
                        html.P('Enter student ID',
                               style={'margin-top': '8px', 'margin-bottom': '4px'},
                               className='font-weight-bold'
                              ),
                        dcc.Input(id='STUDENT_ID', 
                                     style={'width': '320px'}
                                    ),

                    #-----------input text--------------
                        html.P('Input Grate',
                               style={'margin-top': '8px', 'margin-bottom': '4px'},
                               className='font-weight-bold'),
                        dcc.Input(id='Y1T1', type='text',placeholder='เกรดปี 1 เทอม 1',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y1T2', type='text',placeholder='เกรดปี 1 เทอม 2',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y1T3', type='text',placeholder='เกรดปี 1 เทอม 3',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y2T1', type='text',placeholder='เกรดปี 2 เทอม 1',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y2T2', type='text',placeholder='เกรดปี 2 เทอม 2',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y2T3', type='text',placeholder='เกรดปี 2 เทอม 3',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y3T1', type='text',placeholder='เกรดปี 3 เทอม 1',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y3T2', type='text',placeholder='เกรดปี 3 เทอม 2',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y3T3', type='text',placeholder='เกรดปี 3 เทอม 3',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y4T1', type='text',placeholder='เกรดปี 4 เทอม 1',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y4T2', type='text',placeholder='เกรดปี 4 เทอม 2',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        dcc.Input(id='Y4T3', type='text',placeholder='เกรดปี 4 เทอม 3',
                                     style={'width': '320px','margin-bottom': '4px'}),
                        html.Button('Predict', id='button', n_clicks=0)
                    ]
                )
            ],
            style={"height": "50vh"}
        ),
       
    ]
)

content = html.Div(
    [
        dbc.Row(
            [
                dbc.Col(
                    [
                        html.Div([
                            html.P('taget แต่ละปี', className='font-weight-bold'),
                            dcc.Graph(figure=fig_hit)
                    ])

                        ]),
                 dbc.Col(
                    [
                        html.Div([
                            
                            html.Div(id='output')

                        ])
                    ])
            ],
            style={"height": "40vh",'margin-top': '40px'}),
        dbc.Row(
            [
                dbc.Col(
                    [
                        html.P('Correlation Matrix Heatmap')
                    ])
            ],
            style={"height": "50vh"}
            )
        ]
    )

app.layout = dbc.Container(
    [
        dbc.Row(
            [
                dbc.Col(sidebar, width=3, className='bg-light'),
                dbc.Col(content, width=9,className='bg-light')
                ]
            ),
        ],
    fluid=True
    )

@app.callback(
    Output('output', 'children'),
    [Input('button', 'n_clicks')],
    [Input('ADMIT_YEAR','value'),
    Input('MAJOR_ID','value'),
    Input('Y1T1', 'value'),
    Input('Y1T2', 'value'),
    Input('Y1T3', 'value'),
    Input('Y2T1', 'value'),
    Input('Y2T2', 'value'),
    Input('Y2T3', 'value'),
    Input('Y3T1', 'value'),
    Input('Y3T2', 'value'),
    Input('Y3T3', 'value'),
    Input('Y4T1', 'value'),
    Input('Y4T2', 'value'),
    Input('Y4T3', 'value')])

def predict_grade(n_clicks,ADMIT_YEAR,MAJOR_ID,Y1T1, Y1T2,Y1T3 ,Y2T1,Y2T2, Y2T3,Y3T1, Y3T2, Y3T3,Y4T1, Y4T2,Y4T3):
    if not n_clicks:
        return "Enter Grade"
    else:
        input_data = pd.DataFrame({'ADMIT_YEAR': [ADMIT_YEAR] ,'MAJOR_ID': [MAJOR_ID],
            'Y1T1': [Y1T1], 'Y1T2': [Y1T2], 'Y1T3': [Y1T3], 'Y2T1': [Y2T1], 
            'Y2T2': [Y2T2], 'Y2T3': [Y2T3], 'Y3T1': [Y3T1], 'Y3T2': [Y3T2], 
            'Y3T3': [Y3T3], 'Y4T1': [Y4T1], 'Y4T2': [Y4T2], 'Y4T3': [Y4T3]})
        prediction = predict_model(model, data=input_data)
        return f'The predicted class is {prediction["prediction_label"][0]}'



if __name__ == "__main__":
    app.run_server(debug=True, port=1234)