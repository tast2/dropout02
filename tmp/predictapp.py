import dash
from pycaret.classification import *
import pycaret.classification as clf
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output, State
import dash_daq as daq
import dash_bootstrap_components as dbc
import numpy as np
import pandas as pd
import plotly.graph_objs as go
import plotly.express as px
from sklearn.ensemble import RandomForestRegressor
from evidently.dashboard import Dashboard
from evidently.tabs import RegressionPerformanceTab
from evidently.model_profile import Profile
from evidently.profile_sections import RegressionPerformanceProfileSection

data=pd.read_excel('data\\train.xlsx')
major=data['MAJOR_ID'].unique()

# สร้าง Dash application
app = dash.Dash(__name__)
model = clf.load_model('tmp\\test_model1')

# กำหนด layout ของแอปพลิเคชัน
app.layout = html.Div([
    html.Div([
        html.H1(children='PREDICTION',style={'color':'rgb(33 36 35)'}), 
    ], className='side_bar'),
    html.H6("Grade Prediction"),
     html.Div([
        html.Label('Student ID:'),
        dcc.Input(id='STUDENT_ID', type='text', placeholder='Enter student ID'),
    ], style={'display': 'inline-block', 'width': '33.3%'}),
    html.Div([
        html.Label('Admit Year:'),
        dcc.Input(id='ADMIT_YEAR', type='number', placeholder='Enter admit year'),
    ], style={'display': 'inline-block', 'width': '33.3%'}),
    html.Div([
        html.Label('Major ID:'),
        dcc.Dropdown(id='MAJOR_ID', className='dropdown',multi=False,
                     options=major,
                     value=major),
    ], style={'display': 'inline-block', 'width': '33.3%'}),
    html.Div([
        html.Label('English Score:'),
        dcc.Input(id='ENG_SCORE', type='number', placeholder='Enter English score'),
    ], style={'display': 'inline-block', 'width': '33.3%'}),
    html.Div([
        html.Label("Y1T1: "),
        dcc.Input(id='Y1T1', type='number', placeholder='Enter grade 1')
    ],style={'display': 'inline-block', 'width': '16.5%'}),
    html.Div([
        html.Label("Y1T2: "),
        dcc.Input(id='Y1T2', type='number', placeholder='Enter grade 2')
    ],style={'display': 'inline-block', 'width': '16.5%'}),
    html.Div([
        html.Label("Y1T3: "),
        dcc.Input(id='Y1T3', type='number', placeholder='Enter grade 3')
    ]),
    html.Div([
        html.Label("Y2T1: "),
        dcc.Input(id='Y2T1', type='number', placeholder='Enter grade 4')
    ],style={'display': 'inline-block', 'width': '16.5%'}),
    html.Div([
        html.Label("Y2T2: "),
        dcc.Input(id='Y2T2', type='number', placeholder='Enter grade 5')
    ]),
    html.Div([
        html.Label("Y2T3: "),
        dcc.Input(id='Y2T3', type='number', placeholder='Enter grade 6')
    ],style={'display': 'inline-block', 'width': '16.5%'}),
    html.Div([
        html.Label("Y3T1: "),
        dcc.Input(id='Y3T1', type='number', placeholder='Enter grade 7')
    ],style={'display': 'inline-block', 'width': '16.5%'}),
    html.Div([
        html.Label("Y3T2: "),
        dcc.Input(id='Y3T2', type='number', placeholder='Enter grade 8')
    ],style={'display': 'inline-block', 'width': '16.5%'}),
    html.Div([
        html.Label("Y3T3: "),
        dcc.Input(id='Y3T3', type='number', placeholder='Enter grade 9')
    ],style={'display': 'inline-block', 'width': '16.5%'}),
    html.Div([
        html.Label("Y4T1: "),
        dcc.Input(id='Y4T1', type='number', placeholder='Enter grade 10')
    ],style={'display': 'inline-block', 'width': '16.5%'}),
    html.Div([
        html.Label("Y4T2: "),
        dcc.Input(id='Y4T2', type='number', placeholder='Enter grade 11')
    ],style={'display': 'inline-block', 'width': '16.5%'}),
    html.Div([
        html.Label("Y4T3: "),
        dcc.Input(id='Y4T3', type='number', placeholder='Enter grade 12')
    ],style={'display': 'inline-block', 'width': '16.5%'}),
    html.Button('Predict', id='button', n_clicks=0),
    html.Div(id='output')
])

@app.callback(
    Output('output', 'children'),
    [Input('button', 'n_clicks')],
    [Input('ADMIT_YEAR','value'),
    Input('MAJOR_ID','value'),
    Input('Y1T1', 'value'),
    Input('Y1T2', 'value'),
    Input('Y1T3', 'value'),
    Input('Y2T1', 'value'),
    Input('Y2T2', 'value'),
    Input('Y2T3', 'value'),
    Input('Y3T1', 'value'),
    Input('Y3T2', 'value'),
    Input('Y3T3', 'value'),
    Input('Y4T1', 'value'),
    Input('Y4T2', 'value'),
    Input('Y4T3', 'value')])

def predict_grade(n_clicks,ADMIT_YEAR,MAJOR_ID,Y1T1, Y1T2,Y1T3 ,Y2T1,Y2T2, Y2T3,Y3T1, Y3T2, Y3T3,Y4T1, Y4T2,Y4T3):
    if not n_clicks:
        return "Enter Grade"
    else:
        input_data = pd.DataFrame({'ADMIT_YEAR': [ADMIT_YEAR] ,'MAJOR_ID': [MAJOR_ID],
            'Y1T1': [Y1T1], 'Y1T2': [Y1T2], 'Y1T3': [Y1T3], 'Y2T1': [Y2T1], 
            'Y2T2': [Y2T2], 'Y2T3': [Y2T3], 'Y3T1': [Y3T1], 'Y3T2': [Y3T2], 
            'Y3T3': [Y3T3], 'Y4T1': [Y4T1], 'Y4T2': [Y4T2], 'Y4T3': [Y4T3]})
        prediction = predict_model(model, data=input_data)
        return f'The predicted class is {prediction["prediction_label"][0]}'


if __name__ == '__main__':
    app.run_server(debug=True)