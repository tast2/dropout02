import dash_bootstrap_components as dbc
import dash_core_components as dcc
from dash import Input, Output, State, html
from dash_bootstrap_components._components.Container import Container
import dash
import dash_daq as daq
import pandas as pd
import plotly.express as px
from pycaret.classification import *
#------------------------data---------------
data=pd.read_excel('C:\\Users\\User\\Desktop\\pro\\flask_project\\dash_real\\total.xlsx')
major=data['MAJOR_NAME_THAI'].unique()
study=data['ENT_METHOD_DESC'].unique()
grade=data['Y1T1'].unique()
year = {2559: '2559', 2560: '2560', 2561: '2561', 2562: '2562', 2563: '2563', 2564: '2564'}

#-------------------------กราฟ-------------
histogram = px.histogram(data, x='ADMIT_YEAR', color='STUDY_STATUS', marginal="box", nbins=30 ,opacity=0.6, 
                         color_discrete_sequence=['#000066', '#3BA27A'])

barplot =px.histogram(data, x='ADMIT_YEAR', color='STUDY_STATUS', marginal="box", nbins=30 ,opacity=0.6, 
                         color_discrete_sequence=['#FFBD59', '#3BA27A','#000066'])

table = px.histogram(data, x='ADMIT_YEAR', color='STUDY_STATUS', marginal="box", nbins=30 ,opacity=0.6, 
                         color_discrete_sequence=['#FFBD59', '#3BA27A'])


#------------------app------------------------------
app = dash.Dash(__name__)
#---------------ด้านขวา
app.layout = html.Div([
    html.Div([
        html.H1('Grade prediction'),
        html.P("Retirement calculation program"),
        #html.Img(src="assets/tea.png"),
        html.Label('Select major'), 
        dcc.Dropdown(id='class-dropdown', className='dropdown',multi=False,
                     options=major,
                     value=major),
        html.Label('What project do you go to study with?'), 
        dcc.Dropdown(id='gender-dropdown', className='dropdown',multi=False,
                     options=study,
                     value=study),
        html.Button(id='Submit-button',children="Submit")],id='left-container'),
#-----------------ด้านขวา----------------
    html.Div([
        html.Div([
            dcc.Graph(id="histogram", figure=histogram),
            dcc.Graph(id="barplot", figure=barplot)],id='visualisation'),
        html.Div([
            dcc.Graph(id="table", figure=table),
            html.Div([
                html.Label('calculator'), 
                daq.BooleanSwitch(id='target_toggle', className='toggle', on=True),
                html.Label('sort'),
                daq.BooleanSwitch(id='sort_toggle', className='toggle', on=True),
                html.Label('year'), 
                dcc.Slider(id='n-slider', min=2559, max=2564, step=1, value=10,marks=year),
            ],id='table-side'),
        ], id='data-extract')
    ], id='right-container')
], id='container')


#-------------------------------------------------------------------
@app.callback(
    [Output(component_id='histogram', component_property='figure')],
    [State(component_id='class-dropdown', component_property='value'),
     State(component_id='gender-dropdown', component_property='value'),
     Input(component_id='update-button', component_property='n_clicks'),
     Input(component_id='target_toggle', component_property='on'),
     Input(component_id='sort_toggle', component_property='on'),
     Input(component_id='n-slider', component_property='value')]
)
def update_output(class_value, gender_value, n_clicks, target, ascending, n):
    # Update data to dropdown values without overwriting test
    dff = data.copy()

    if n_clicks>0:
        if len(class_value)>0:
            dff = dff[dff['Class'].isin(class_value)]
        elif len(class_value)==0:
            raise dash.exceptions.PreventUpdate
        
        if len(gender_value)>0:
            dff = dff[dff['Gender'].isin(gender_value)]
        elif len(gender_value)==0:
            raise dash.exceptions.PreventUpdate

# Visual 1: Histogram
    histogram = px.histogram(dff, x='Probability', color='STUDY_STATUS', marginal="box", nbins=30, 
                             opacity=0.6, color_discrete_sequence=['#FFBD59', '#3BA27A'])
    histogram.update_layout(title_text=f'Distribution of probabilities by class (n={len(dff)})',
                            font_family='Tahoma', plot_bgcolor='rgba(255,242,204,100)')
    histogram.update_yaxes(title_text="Count")

    return histogram

if __name__ == '__main__':
    app.run_server(debug=True)