import numpy as np
import pickle
import plotly 



class DropOut :

    def __init__(self) :
        self.__grade=np.full((1,12), np.nan)
        self.__data={}
        self.year=1
        self.term=1
        self.__model=importModel()
        self.__result=''

    def importModel(self) :
        with open('model_name.lol','rb') as f :
            model=pickle.load(f)
        return model
        
    def StudentData(self,code=np.nan,name=None,major=np.nan,admit=np.nan,Year=1,Term=1) :
        self.__data={'STcode':code,
                'NAME':name,
                'MAJOR_ID':major,
                'ADMIT_year':admit}
        self.year=Year
        self.term=Term
        if self.year>4 :
            self.year=4
        elif self.year<1 :
            self.year=1

        if self.term>3 :
            self.term=3
        elif self.term<1 :
            self.term=1       
    
    def studentGrade(self,grade=[]) :
        T=0
        for value in grade :
            if value=='-' :
                self.__grade[:,T]=None
            elif value>4 or value<0 :
                value=np.nan
                self.__grade[:,T]=value
            else :
                self.__grade[:,T]=value
            T+=1

        value_Y=1 ; value_T=0
        for i in self.__grade[0] :
            value_T+=1
            if value_T==4 :
                value_T=1
                value_Y+=1
            self.__data[f'Y{value_Y}T{value_T}']=i
    
    def modelOperater(self) :
        self.__result=self.__model(self.__grade)

    def modelResult(self) :
        return self.__result